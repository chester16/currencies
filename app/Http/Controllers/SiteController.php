<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index(){
        $client = new Client();
        $uri = 'http://phisix-api3.appspot.com/stocks.json';
        $result = $client->get($uri);

        $currencies = json_encode(json_decode($result->getBody()->getContents())->stock);

        return view('welcome',compact('currencies'));
    }
}
